package com.example;

import ai.ecma.codingbat.entity.template.AbsTitleIntegerEntity;
import ai.ecma.codingbat.util.CommonUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;


@Getter
@NoArgsConstructor
public final class Language extends AbsTitleIntegerEntity {

    private String url;


    public Language(String title) {
        setTitle(title);
    }

    public Language(String title, String url) {
        setTitle(title);
        this.url = url;
    }

    public Language(String title, String url, Integer id) {
        this(title,url);
        setId(id);
    }



    private void setUrl() {
        this.url = CommonUtils.makeUrl(super.getTitle());
    }

    @Override
    public void setTitle(String title) {
        super.setTitle(title);
        setUrl();
    }
}
